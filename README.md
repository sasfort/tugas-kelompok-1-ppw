[![pipeline status](https://gitlab.com/sasfort/tugas-kelompok-1-ppw/badges/master/pipeline.svg)](https://gitlab.com/sasfort/tugas-kelompok-1-ppw/-/commits/master)
[![coverage report](https://gitlab.com/sasfort/tugas-kelompok-1-ppw/badges/master/coverage.svg)](https://gitlab.com/sasfort/tugas-kelompok-1-ppw/-/commits/master)

![logo](pepewsFootwear/static/logo.png) 

# Tugas Kelompok 1 Perancangan dan Pemrograman Web

This repository contains the file made as Perancangan dan Pemrograman Web group assignment.

## Ownership

This is the repository that contains works of:

Nama                                 | NPM
------------------------------------ | -------------
Samuel                               | 1906285592
Syabib Ash-Shiddiqi                  | 1906350686
Geoffrey Tyndall                     | 1906350704
Johanes Marihot Perkasa Simarmata    | 1906308476
Michael Eliazer Intan                | 1806191124

## High Fidelity Prototype

The high fidelity prototype for this project can be found on https://www.figma.com/file/5l98ofdT1QCTp2Wfa9Kb5H/TK-1-PPW?node-id=0%3A1

## Persona

The user persona for this website can be found on https://www.figma.com/file/pDsdqc484jROcxpQgPfba5/Personas-TK-1?node-id=0%3A1

## Wireframe

The wireframes for this website can be found on:

#### Landing Page
https://wireframe.cc/4R1irc

#### Product Page
https://wireframe.cc/ii2uTu

#### Category Page
https://wireframe.cc/m77T4F

#### Transaction Page
https://wireframe.cc/GUMSPC

#### Review Page
https://wireframe.cc/tLUWaW

#### Coupon Page
https://wireframe.cc/yIqXoC

## Heroku

Our website can be found on https://ppw-tk1.herokuapp.com/