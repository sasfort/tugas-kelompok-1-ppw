from django import forms
from .models import *
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User


class TransactionCreateForm(forms.ModelForm):
    class Meta:
        model = Transaction
        fields = '__all__'
        widgets = {
            'orderId': forms.HiddenInput(attrs={
                'class': 'form-control'
            }),
            'customer': forms.TextInput(attrs={
                'class': 'form-control'
            }),
            'produk': forms.HiddenInput(attrs={
                'class': 'form-control'
            }),
            'transDate': forms.HiddenInput(attrs={
                'class': 'form-control'
            }),
            'noHP': forms.TextInput(attrs={
                'class': 'form-control'
            }),
            'alamat': forms.Textarea(attrs={
                'class': 'form-control'
            }),
        }


class ReviewPageCreateForm(forms.Form):
    pilihanRate = (
        (1, 1),
        (2, 2),
        (3, 3),
        (4, 4),
        (5, 5),
    )
    headline = forms.CharField(required = False ,widget= forms.TextInput(attrs={
        'class': 'form-control',
        'type': 'text',
        'placeholder': 'Plese enter your headline',
    }))
    ulasan = forms.CharField(required = False, widget= forms.Textarea(attrs={
        'class': 'form-control',
        'type': 'text',
        'placeholder': 'Please enter your review',
    }))
    bintang = forms.IntegerField(required=False,widget= forms.Select(choices = pilihanRate,attrs={
        'class': 'form-control',
        'placeholder' : 'Please enter the star',
    }))


class SearchForm(forms.Form):
    coupon = forms.CharField(widget=forms.TextInput(attrs={'class':'searchbox', 'type':'text', 'id':'listinput'}))


class PaymentForm(forms.Form):
    choices = [
        ("Debit card","Debit Card") ,
        ("Credit card","Credit card"),
    ]
    payment = forms.ChoiceField(widget=forms.RadioSelect(attrs={}), choices=choices)
    card_number = forms.CharField(widget=forms.TextInput(attrs={'class':'form-control'}))


class loginForm(forms.Form):
    username = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'type': 'text',
        'title': 'Username',
        'placeholder': 'Plese enter your username',
        'required': True,
    }))
    password = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'type': 'password',
        'placeholder': 'Plese enter your password',
        'required': True,
    }))


class SignUpForm(UserCreationForm):
    first_name = forms.CharField(
        max_length=30, required=True, help_text='Enter your first name.')
    last_name = forms.CharField(
        max_length=30, required=True, help_text='Enter your last name.')
    email = forms.EmailField(
        max_length=254, help_text='Required. Inform a valid email address.')

    class Meta:
        model = User
        fields = ('username', 'first_name', 'last_name',
                  'email', 'password1', 'password2', )
