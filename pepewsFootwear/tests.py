from django.test import TestCase

# Create your tests here.
from django.test import TestCase, Client, LiveServerTestCase
from django.urls import resolve, reverse

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver import DesiredCapabilities
from selenium.webdriver.chrome.options import Options

from .models import *
from .views import *

import time
import os

# Create your tests here.


class PepewsFootwearTest(TestCase):

    def test_product_page_url_is_exist(self):
        Style.objects.create(nama='Classy')
        Product.objects.create(nama='Test', harga=1000000, deskripsi='This is a test',
                               stok=25, style=Style.objects.get(id=1), gambar='https://i.imgur.com/SkKfJ9J.png')
        response = Client().get('/product/1')
        self.assertEqual(response.status_code, 200)

    def test_product_page_using_indexProduct_function(self):
        Style.objects.create(nama='Classy')
        Product.objects.create(nama='Test', harga=1000000, deskripsi='This is a test',
                               stok=25, style=Style.objects.get(id=1), gambar='https://i.imgur.com/SkKfJ9J.png')
        found = resolve('/product/1')
        self.assertEqual(found.func, indexProduct)

    def test_style_model(self):
        Style.objects.create(nama='Classy')
        count = Style.objects.all().count()
        self.assertEqual(count, 1)

    def test_product_model(self):
        Style.objects.create(nama='Classy')
        Product.objects.create(nama='Test', harga=1000000, deskripsi='This is a test',
                               stok=25, style=Style.objects.get(id=1), gambar='https://i.imgur.com/SkKfJ9J.png')
        count = Product.objects.all().count()
        self.assertEqual(count, 1)

    def test_product_page_is_not_none(self):
        self.assertIsNotNone(indexProduct)

    def test_product_page_is_using_correct_html(self):
        Style.objects.create(nama='Classy')
        Product.objects.create(nama='Test', harga=1000000, deskripsi='This is a test',
                               stok=25, style=Style.objects.get(id=1), gambar='https://i.imgur.com/SkKfJ9J.png')
        response = Client().get('/product/1')
        self.assertTemplateUsed(response, 'productPage.html')

    def test_landing_page_is_using_correct_html(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'landingPageNew.html')

    def test_transaction_page_is_using_correct_html(self):
        response = Client().get('/transaction/')
        self.assertTemplateUsed(response, 'transactionPage.html')

    def test_classy_page_is_using_correct_html(self):
        response = Client().get('/classy/')
        self.assertTemplateUsed(response, 'classy.html')

    def test_trendy_page_is_using_correct_html(self):
        response = Client().get('/trendy/')
        self.assertTemplateUsed(response, 'trendy.html')

    def test_sporty_page_is_using_correct_html(self):
        response = Client().get('/sporty/')
        self.assertTemplateUsed(response, 'sporty.html')

    # def test_review_page_is_using_correct_html(self):
    #     Style.objects.create(nama='Classy')
    #     Product.objects.create(nama='Test', harga=1000000, deskripsi='This is a test',
    #                            stok=25, style=Style.objects.get(id=1), gambar='https://i.imgur.com/SkKfJ9J.png')
    #     Review.objects.create(produk=Product.objects.get(
    #         id=1), headline='test', ulasan='testtest', bintang=5, pengulas='tester', tanggal=date.today())
    #     response = Client().get('/review/1')
    #     self.assertTemplateUsed(response, 'reviewPage.html')

    def test_coupon_page_is_using_correct_html(self):
        response = Client().get('/coupon/')
        self.assertTemplateUsed(response, 'coupon.html')

    # Authentication

    def test_login_page_url_is_exist(self):
        response = Client().get('/login/')
        self.assertEqual(response.status_code, 200)

    def test_login_page_is_using_login_function(self):
        found = resolve('/login/')
        self.assertEqual(found.func, loginFunc)

    def test_login_page_is_using_correct_html(self):
        response = Client().get('/login/')
        self.assertTemplateUsed(response, 'login.html')

    def test_signup_page_url_is_exist(self):
        response = Client().get('/login/')
        self.assertEqual(response.status_code, 200)

    def test_signup_page_is_using_signup_function(self):
        found = resolve('/signup/')
        self.assertEqual(found.func, signup)

    def test_signup_page_is_using_correct_html(self):
        response = Client().get('/signup/')
        self.assertTemplateUsed(response, 'signup.html')

    def test_project_coupon_GET(self):
        client = Client()
        response = client.get(reverse('coupon'))

        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, 'coupon.html')
    
    def test_project_payment_GET(self):
        client= Client()
        response = client.get(reverse('payment'))

        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, 'payment.html')


class PepewsFootwearFuncTests(LiveServerTestCase):

    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')

        self.selenium = webdriver.Chrome(
            './chromedriver', chrome_options=chrome_options)
        self.selenium.get(self.live_server_url + '/login/')
        super(PepewsFootwearFuncTests, self).setUp()

    def test_sign_up_log_out_wrong_password_and_log_in(self):
        selenium = self.selenium

        selenium.implicitly_wait(5)

        sign_up_link = selenium.find_element_by_id('sign-up')
        sign_up_link.click()

        selenium.implicitly_wait(5)

        username_textbox = selenium.find_element_by_id('id_username')
        first_name_textbox = selenium.find_element_by_id('id_first_name')
        last_name_textbox = selenium.find_element_by_id('id_last_name')
        email_textbox = selenium.find_element_by_id('id_email')
        password_textbox = selenium.find_element_by_id('id_password1')
        conf_password_textbox = selenium.find_element_by_id('id_password2')

        submit_button = selenium.find_element_by_id('submit-button')

        username_textbox.send_keys("testAcc1")
        first_name_textbox.send_keys("Test")
        last_name_textbox.send_keys("Account")
        email_textbox.send_keys("testAcc@test.com")
        password_textbox.send_keys("ppwtk2auth")
        conf_password_textbox.send_keys("ppwtk2auth")

        submit_button.click()

        selenium.implicitly_wait(5)

        logout_button = selenium.find_element_by_id('logout')
        logout_button.click()

        selenium.implicitly_wait(5)

        login_username_textbox = selenium.find_element_by_id('id_username')
        login_password_textbox = selenium.find_element_by_id('id_password')

        login_submit_button = selenium.find_element_by_id('submit')

        login_username_textbox.send_keys("testAcc1")
        login_password_textbox.send_keys("testWrongPassword")

        login_submit_button.click()

        selenium.implicitly_wait(5)

        login_username_textbox = selenium.find_element_by_id('id_username')
        login_password_textbox = selenium.find_element_by_id('id_password')

        login_username_textbox.clear()
        login_password_textbox.clear()

        login_submit_button = selenium.find_element_by_id('submit')

        login_username_textbox.send_keys("testAcc1")
        login_password_textbox.send_keys("ppwtk2auth")

        login_submit_button.click()

        selenium.implicitly_wait(5)

        username_display = selenium.find_element_by_id('user-display').text

        self.assertEqual(username_display, "Hello, TestAcc1")

    def test_payment(self):
        self.selenium.get(self.live_server_url + '/payment/')
        self.selenium.implicitly_wait(3)
        search = self.selenium.find_element_by_id('user-input')

        search.send_keys('30')
        

    def tearDown(self):
        self.selenium.quit()
        super(PepewsFootwearFuncTests, self).tearDown()
