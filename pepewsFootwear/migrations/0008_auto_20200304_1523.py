# Generated by Django 2.2.10 on 2020-03-04 15:23

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('pepewsFootwear', '0007_auto_20200304_1417'),
    ]

    operations = [
        migrations.AddField(
            model_name='transaction',
            name='alamat',
            field=models.TextField(default='Jalan Isaja Hidupmu'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='transaction',
            name='noHP',
            field=models.CharField(default='0812345678', max_length=12),
            preserve_default=False,
        ),
    ]
