# Generated by Django 2.2.10 on 2020-03-04 14:16

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('pepewsFootwear', '0005_auto_20200304_1402'),
    ]

    operations = [
        migrations.AddField(
            model_name='review',
            name='headline',
            field=models.CharField(max_length=150, null=True),
        ),
    ]
