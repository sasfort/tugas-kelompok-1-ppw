# Generated by Django 3.0.3 on 2020-03-04 10:30

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('pepewsFootwear', '0003_auto_20200304_1026'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='product',
            name='transaksi',
        ),
        migrations.AddField(
            model_name='transaction',
            name='produk',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='pepewsFootwear.Product'),
        ),
    ]
