from django.contrib import admin
from .models import *

# Register your models here.
admin.site.register(Product)
admin.site.register(Style)
admin.site.register(Transaction)
admin.site.register(Review)
admin.site.register(Coupon)