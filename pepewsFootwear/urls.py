from django.conf.urls import url
from django.contrib import admin
from django.urls import path, register_converter
from django.conf.urls.static import static
from .views import *

urlpatterns = [
    path('', indexMain, name='indexMain'),
    path('product/<str:pk>', indexProduct, name='indexProduct'),
    path('classy/', classy, name='classy'),
    path('sporty/', sporty, name='sporty'),
    path('trendy/', trendy, name='trendy'),
    path('transaction/', indexTransaction, name='indexTransaction'),
    path('review/<str:pk>', indexReview, name='indexReview'),
    path('postReview/<str:pk>', postReview, name='postReview'),
    path('coupon/', coupon, name='coupon'),
    path('payment/', payment, name='payment'),
    path('login/', loginFunc, name='login'),
    path('logout/', logoutFunc, name='logout'),
    path('signup/', signup, name='signup'),
    # path('test/', testing, name='testing'),
]
