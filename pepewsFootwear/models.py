from django.db import models
from django.core.validators import *
from django.db.models.signals import post_delete
from django.dispatch import receiver
from django.contrib.auth.models import User

# Create your models here.


class Style(models.Model):
    pilihanStyle = (
        ('Classy', 'Classy'),
        ('Trendy', 'Trendy'),
        ('Sporty', 'Sporty')
    )
    nama = models.CharField(max_length=120, choices=pilihanStyle)

    def __str__(self):
        return self.nama


class Product(models.Model):

    nama = models.CharField(max_length=120)
    harga = models.IntegerField()
    deskripsi = models.TextField()
    style = models.ForeignKey(Style, null=True, on_delete=models.SET_NULL)
    stok = models.IntegerField()
    gambar = models.TextField()

    def __str__(self):
        return self.nama


class Review(models.Model):

    pilihanRate = (
        (1, 1),
        (2, 2),
        (3, 3),
        (4, 4),
        (5, 5),
    )
    produk = models.ForeignKey(Product, null=True, on_delete=models.SET_NULL)
    pengulas = models.ForeignKey(User, on_delete=models.CASCADE)
    headline = models.CharField(max_length=150)
    ulasan = models.TextField()
    bintang = models.IntegerField(choices=pilihanRate, default=5)
    tanggal = models.DateField(auto_now_add=True)

    def __str__(self):
        return self.headline + " "+ self.produk.nama


class Transaction(models.Model):

    orderId = models.CharField(max_length=120)
    customer = models.CharField(max_length=120)
    transDate = models.DateTimeField(auto_now=True)
    produk = models.ForeignKey(Product, null=True, on_delete=models.SET_NULL)
    noHP = models.CharField(max_length=12)
    alamat = models.TextField()

    def __str__(self):
        return self.customer


class Coupon(models.Model):
    nama = models.CharField(max_length=100, null=True)
    deskripsi = models.TextField(null=True)
    kode = models.CharField(max_length=100)
    persen = models.CharField(max_length=100)
    tanggal = models.DateField()
    harga = models.IntegerField(default=10000)

    def __str__(self):
        return '{}'.format(self.nama)
