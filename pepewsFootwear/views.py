from django.shortcuts import render, redirect
from .models import *
from .forms import *
from datetime import datetime, date
from django.contrib import messages
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.forms import UserCreationForm
from django.db.models import Q
from django.template.loader import render_to_string
from django.http import JsonResponse

# Create your views here.


def indexMain(request):
    return render(request, 'landingPageNew.html')


def indexProduct(request, pk):
    selectedProduct = Product.objects.get(id=pk)
    orderIdTmp = datetime.now().strftime("%m%d%Y%H%M%S")+selectedProduct.nama
    print(orderIdTmp)
    initial_data = {
        'orderId': orderIdTmp,
        'transDate': datetime.now(),
        'produk': selectedProduct,
    }
    form = TransactionCreateForm(request.POST or None, initial=initial_data)
    print(initial_data)
    reviews = Review.objects.filter(produk=selectedProduct)
    if form.is_valid():
        print('save berhasil')
        form.save()
        selectedProduct.stok -= 1
        selectedProduct.save()
        messages.success(request, 'Produk berhasil dipesan')
    else:
        print(form.errors)
        print("gagal")

    context = {
        'selectedProduct': selectedProduct,
        'form': form,
        'reviews': reviews,
    }
    return render(request, 'productPage.html', context)


def indexTransaction(request):
    transactions = Transaction.objects.all()

    context = {
        'transactions': transactions,
    }
    return render(request, 'transactionPage.html', context)


def classy(request):
    return render(request, 'classy.html', {'classy_list': Product.objects.filter(style__nama__contains='Classy')})


def sporty(request):
    return render(request, 'sporty.html', {'sporty_list': Product.objects.filter(style__nama__contains='Sporty')})


def trendy(request):
    return render(request, 'trendy.html', {'trendy_list': Product.objects.filter(style__nama__contains='Trendy')})


def testing(request):
    return render(request, 'test.html')


def indexReview(request, pk):
    user = request.user
    form = ReviewPageCreateForm()
    selectedProduct = Product.objects.get(id=pk)
    data = {
        'produk': selectedProduct
    }
    review = Review.objects.filter(produk=selectedProduct)
    total = 0
    jumlah_bintang_1 = 0
    jumlah_bintang_2 = 0
    jumlah_bintang_3 = 0
    jumlah_bintang_4 = 0
    jumlah_bintang_5 = 0
    for rev in review:
        total += rev.bintang
        if rev.bintang == 1:
            jumlah_bintang_1 += 1
        elif rev.bintang == 2:
            jumlah_bintang_2 += 1
        elif rev.bintang == 3:
            jumlah_bintang_3 += 1
        elif rev.bintang == 4:
            jumlah_bintang_4 += 1
        elif rev.bintang == 5:
            jumlah_bintang_5 += 1
    try:
        avg = total / len(review)
        avg = round(avg, 2)
    except ZeroDivisionError:
        avg = 0
    avg = round(avg, 2)
    context = {
        'selectedProduct': selectedProduct,
        'review': review,
        'average': avg,
        'form' : form,
        'postPK' : pk,
        'user' : user,
        'bintang1': jumlah_bintang_1,
        'bintang2': jumlah_bintang_2,
        'bintang3': jumlah_bintang_3,
        'bintang4': jumlah_bintang_4,
        'bintang5': jumlah_bintang_5

    }
    return render(request, 'reviewPage.html', context)

def postReview(request, pk) :
    selectedProduct = Product.objects.get(id=pk)
    data ={
        'produk' : selectedProduct
    }
    response_data = {}
    if request.method == "POST":
        print(request.POST)
        headline = request.POST.get('headline')
        ulasan = request.POST.get('ulasan')
        bintang = request.POST.get('bintang')

        response_data['headline'] = headline
        response_data['ulasan'] = ulasan
        response_data['bintang'] = bintang
        response_data['tanggal'] = datetime.now().strftime("%m-%d-%Y")

        Review.objects.create(produk = selectedProduct, pengulas = request.user,
        headline = headline, ulasan = ulasan, bintang = bintang)
        return JsonResponse(response_data)

def coupon(request):
    form = SearchForm()
    if 'coupon' in request.GET:
        result = str(request.GET['coupon']).lower()
        coupons = list(Coupon.objects.all())
        hasil = []
        for i in coupons:
            if (result in i.nama.lower() or result in str(i.persen) or result in i.kode.lower()):
                hasil.append(i)        
        return render(request, 'coupon.html',{"form":form, "hasil": hasil,})

    return (render(request, 'coupon.html',{'coupon': list(Coupon.objects.all()), 'form':form }))


def loginFunc(request):
    form = loginForm()
    if(request.method == "POST"):
        form = loginForm(request.POST)
        if(form.is_valid()):
            username = form.cleaned_data['username']
            password = form.cleaned_data['password']
            user = authenticate(request, username=username, password=password)
            if(user is not None):
                login(request, user)
                return redirect('/login/')
            else:
                messages = ['Incorrect username or password']
                response = {'form': form, 'messages': messages}
                return render(request, 'login.html', response)
    else:
        response = {'form': form}
        return render(request, 'login.html', response)


def logoutFunc(request):
    logout(request)
    return redirect('/login/')


def signup(request):
    if request.method == 'POST':
        form = SignUpForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            raw_password = form.cleaned_data.get('password1')
            user = authenticate(username=username, password=raw_password)
            login(request, user)
            return redirect('/login/')
    else:
        form = SignUpForm()
    return render(request, 'signup.html', {'form': form})

def payment(request):
    # Instantiate object of searhform and paymentform
    paymentForm = PaymentForm()
    # Setting up context for template
    context = {'paymentForm':paymentForm}
    # Check if query q exists
    url_parameter = str(request.GET.get('q'))
    print(request)
    print("ini url_parameter " + url_parameter)
    
    # Define variable coupons
    coupons = []
    if url_parameter:
        print("ini nyari berdasarkan q")
        counter =0
        for coupon in Coupon.objects.all():
            if coupon.persen >= url_parameter:
                coupons.append((coupon, counter))
                counter +=1  
            if len(coupons) >= 3:
                break

    # Adding coupons to context
    if coupons:
        context["coupons"] = coupons
    print(context)
    print("ini semua coupon ", Coupon.objects.all())

    # Handle AJAX request
    if request.is_ajax():
        print("masuk if ajax")
        html = render_to_string(template_name='couponajax.html', context={'coupons':coupons})
        print(html)
        data = {'html':html}
        return JsonResponse(data=data, safe=False)

    return render(request,'payment.html', context=context)
